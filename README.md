# Front-End-New
SmartPlanting front_end
## Important Notice: 
```
DON'T "Download ZIP". Otherwise, main/views/yolov5_optimized/16x2.pt will NOT work properly! 
Instead, use "Clone"
```

### Instruction:
#### Inside this Front-End-New directory:
```
python manage.py makemigrations
python manage.py migrate
python manage.py shell (use Control-D to exit)
python manage.py runserver (If exit, use CONTROL+C)
```

### Install required package:
```
pip install PACKAGE-NAME
pip install numpy
pip install attrs==19.3.0
pip install matplotlib
pip install djangorestframework
pip install django-cors-headers
pip install django-storages

pip install opencv-python
pip install pandas
pip install PyYAML
pip install tqdm
pip install seaborn

pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cpu
(pytorch - HCP2 server doesn't have a GPU)
```

### AWS info:
```
Public IPv4 address
54.177.214.223

User name
ubuntu

GitHub remote url address
https://github.com/VisionX-FullStack/Front-End
```

### AWS instruction:
```
git remote -v
git fetch origin main
git status
git diff origin
git stash
git merge origin/main
git stash pop
git add .
git commit -m "update message" --author="name <email address>"
git pull
git push

sudo systemctl restart nginx
```
