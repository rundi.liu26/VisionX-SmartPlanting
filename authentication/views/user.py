from django.views.generic import ListView
from re import template
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect, HttpResponse
from django.urls import reverse
from requests import request
from django.contrib import messages
from authentication.models import User
from main.models import Cart
from django.db.models import Q
from django.views.generic import UpdateView
from authentication.utils.check_login_state import check_login



def login(request):
    if request.method == "POST":
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        message = "Fill out all of fields!"
        if username and password:
            username = username.strip()
            try:
                user = User.objects.get(name=username)
                if user.password == password:
                    request.session['user_id'] = user.id
                    request.session['username'] = username
                    return redirect('main:index')
                else:
                    message = "Wrong password！"
            except:
                message = "Username doesn't exist！"
        return render(request, 'authentication/login.html', {"message": message})
    return render(request, 'authentication/login.html')

@check_login
def update(request):
    id = request.session['user_id']
    user = User.objects.get(id=id)
    if request.method == "POST":
        message = "Fill out all of fields!"
        try:
            old_name = request.POST.get('old_name', None)
            new_name = request.POST.get('new_name', None)
            old_email = request.POST.get('old_email', None)
            new_email = request.POST.get('new_email', None)
            user = User.objects.get(name=old_name)
            
            if new_name and new_email:
                new_name = new_name.strip()
                user_list = User.objects.filter(Q(name=new_name) | Q(email=new_email))
                if user_list.exists():
                    message = 'New username or email exists!'
                if user.email == old_email:
                    user.name = new_name
                    user.email = new_email
                    user.save()
                    message = "Updated"
                    return redirect('authentication:login')
                else:
                    message = 'Old username and old email do not match!'
        except Exception as e:
            if old_email and old_name:
                message = "Old username or email not found！"
        return render(request, 'authentication/update.html', {"message": message}, {"user":user})
    return render(request, 'authentication/update.html', {"user":user})

@check_login
def logout(request):
    if 'user_id' in request.session:
        del request.session['user_id']
    if 'username' in request.session:
        del request.session['username']
    return redirect('main:index')


def register(request):
    if request.method == "POST":
        username = request.POST.get('username', None)
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        message = "Fill out all of fields!"
        if username and password and email:
            username = username.strip()
            user_list = User.objects.filter(Q(name=username) | Q(email=email))
            if user_list.exists():
                message = "Username or email exists！"
            else:
                user = User(name=username, password=password, email=email)
                user.save()
                request.session['user_id'] = user.id
                request.session['username'] = username
                return redirect('main:index')
        return render(request, 'authentication/register.html', {"message": message})
    return render(request, 'authentication/register.html')


@check_login
def userinfo(request, username):
    user = get_object_or_404(User, pk=request.session['user_id'])
    return render(request, 'authentication/user.html', {'user': user})


@check_login
def check_cart(request):
    cart = {'empty': False}

    cart_list = Cart.objects.filter(Q(id=request.session['user_id']))
    if not cart_list.exists():
        cart['empty'] = True
        return HttpResponse(cart)
    # else:
    #     user = User(name=username, password=password, email=email)
    #     user.save()
    #     request.session['user_id'] = user.id
    #     request.session['username'] = username
    #     return redirect('main:index')

class update_profile(UpdateView):
    model = User
    template_name = 'authentication/update.html'
    fields = '__all__'
