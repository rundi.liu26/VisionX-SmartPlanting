from django.urls import path

from authentication.views import user
from authentication.views.user import update_profile

app_name = 'authentication'
urlpatterns = [
    path('update/<int:pk>', update_profile.as_view(), name='update'),
    path('login/', user.login, name='login'),
    path('logout/', user.logout, name='logout'),
    path('register/', user.register, name='register'),
    path('checkcart/', user.check_cart, name='check_cart'),
    path('<str:username>/', user.userinfo, name='user'),
]
