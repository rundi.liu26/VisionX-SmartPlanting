import datetime
from email.policy import default
from django.db import models
from django.urls import reverse


class User(models.Model):
    name = models.CharField(max_length=128, unique=True)
    password = models.CharField(max_length=256)
    email = models.EmailField(unique=True)
    profile = models.ImageField(upload_to='user_image/', default='user_image/no_image.jpeg')
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
        ('Prefer not to say',  'Prefer not to say'),
    )
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, default='Male')
    birthday = models.DateField(default = datetime.datetime.today().strftime("%Y-%m-%d"))
    phone_num = models.PositiveBigIntegerField(default=0000000000)
    c_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('authentication:login')
    class Meta:
        ordering = ['c_time']

    


