import collections
from unicodedata import name
from attr import fields
from django import forms
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView,DeleteView,CreateView
import logging
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from authentication.utils.check_login_state import check_login
from main.serializer import CartItemSerializer
from main.utils.ai_solutions_utils import *
from main.models import Cart, CartItem, Post
from main.models import Comment
from authentication.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
#from authentication.form import CommentForm

logger = logging.getLogger(__name__)
def index(request):
    return render(request, 'main/consulting.html', {})

sort=0 
"""
0: regular 
1: reverse
2: hot
"""


class HomeView(ListView):
    model = Post
    template_name = 'main/consulting.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['object_list'] = context['object_list'][::-1]
        print(context)
        return context
        
    
#@method_decorator(check_login, name='dispatch')
class PostDetail(DeleteView):
    model = Post
    template_name = 'main/post_detail.html'

    def dispatch(self, request, *args, **kwargs):
        try:
            self.user = get_object_or_404(User, pk=request.session['user_id'])
        except:
            self.user = user = User.objects.get(name='Guest')
        
        self.p = get_object_or_404(Post, id=self.kwargs['pk'])
        if not self.p.author:
            self.p.author = self.user
            # send post to our email
            send_mail(
                "Post update",
                f"Hello, \n\nThe user: {self.p.author.name}({self.p.author.email}) has shared a post.\nPost title: {self.p.title}\nPost content: {self.p.content}\nPlease check the detail at Smartplanting.net as admin.\n\nThank you!",
                "rundi.liu26@gmail.com",
                ["rundi.liu26@visionx.org"]
            )
        self.p.save()

        Comment.objects.filter(name=None).update(name=self.user.name)
        
        self.p.post_views = self.p.post_views + 1
        self.p.save()
        return super(PostDetail, self).dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(PostDetail, self).get_context_data(**kwargs)
        ob = get_object_or_404(Post, id=self.kwargs['pk'])
        num = ob.num_likes
        context["num_likes"] = num
        return context

#@method_decorator(check_login, name='dispatch')
class CreatePost(CreateView):
    model = Post
    template_name: str = 'main/create_post.html' # same w render(request, teplate_name)
    fields = ['title', 'image', 'content']

    def dispatch(self, request, *args, **kwargs):
        return super(CreatePost, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

#@method_decorator(check_login, name='dispatch')
class AddComment(CreateView):
    model = Comment
    #form_class = CommentForm
    template_name = 'main/add_comment.html'
    fields = ['post', 'content']

    def get_context_data(self, **kwargs):
        context = super(AddComment, self).get_context_data(**kwargs)
        ob = get_object_or_404(Post, id=self.kwargs['pk'])
        context["form"].fields["post"].queryset = Post.objects.filter(title=ob.title, author=ob.author)
        return context
    
class Experts(ListView):
    model = Post
    template_name = 'main/experts.html'
    fields = '__all__'

class MyShare(ListView):
    model = Post
    template_name = 'main/my_share.html'

@check_login
def LikeView(request, pk):
    post = get_object_or_404(Post, id=request.POST.get('post_id')) # grab in detail html
    try:
        user = get_object_or_404(User, pk=request.session['user_id'])
    except:
        user = User.objects.get(name='Guest')
    if user not in post.likes.all():
        post.likes.add(user)
    else:
        post.likes.remove(user)
    return HttpResponseRedirect(reverse('main:post_detail', args=[str(pk)]))


class SortedView(ListView):
    model = Post
    template_name = 'main/consulting.html'

    def get_context_data(self, **kwargs):
        context = super(SortedView, self).get_context_data(**kwargs)
        return context

class ReverseView(ListView):
    model = Post
    template_name = 'main/consulting.html'

    def get_context_data(self, **kwargs):
        context = super(ReverseView, self).get_context_data(**kwargs)
        new_context = context
        new_context['object_list'] = new_context['object_list'].order_by('-date_added')
        return new_context

class HotView(ListView):
    model = Post
    template_name = 'main/consulting.html'

    def get_context_data(self, **kwargs):
        context = super(HotView, self).get_context_data(**kwargs)
        hot_context = context
        hot_context['object_list'] = hot_context['object_list'].order_by('-post_views')
        return hot_context

class CartView(APIView):
    
    def get(self, request):
        user = request.user
        cart = Cart.objects.filter(user=user,ordered=False).first()
        queryset = CartItem.objects.filter(cart=cart)
        serializer = CartItemSerializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request):
        data = request.data
        user = request.user
        cart, _ = Cart.objects.get_or_create(user=user, ordered=False)
        product = Product.objects.get(id=data.get('product'))
        price = product.price
        quantity = data.get('quantity')
        cart_items = CartItem(cart=cart, user=user, product=product, price=price, quantity=quantity)
        cart_items.save()

        cart_items = CartItem.objects.filter(user=user, cart=cart.id)
        cart.total_price = sum([i for i in cart_items])
        cart.save()

        return Response({'success': 'Items Added'})

    def put(self, request):
        data = request.data
        cart_item = CartItem.objects.get(id=data.get('id'))
        quantity = data.get('quantity')
        cart_item.quantity += quantity
        cart_item.save()
        return Response({'success': 'Items Updated'})

    def delete(self, request):
        user = request.user
        data = request.data
        cart_item = CartItem.objects.get(id=data.get('id'))
        cart_item.delete()
        cart = Cart.objects.filter(user=user, ordered=False).first()
        queryset = CartItem.objects.filter(cart=cart)
        serialier = CartItemSerializer(queryset, many=True)
        return Response(serialier.data)

@method_decorator(check_login, name='dispatch')
class shopping_carts(ListView):
    model = CartItem
    template_name = 'main/shopping_carts.html'

    def dispatch(self, request, *args, **kwargs):
        try:
            self.user = get_object_or_404(User, pk=request.session['user_id'])
        except:
            self.user = user = User.objects.get(name='Guest')
        return super(shopping_carts, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs['object_list'] = CartItem.objects.order_by('id')
        return super(shopping_carts, self).get_context_data(**kwargs)


