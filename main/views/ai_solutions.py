from django.shortcuts import render

import logging

from authentication.utils.check_login_state import check_login
from main.utils.ai_solutions_utils import *

logger = logging.getLogger(__name__)


def index(request):
    return render(request, 'main/index.html')


#@check_login
def ai_solutions(request, page):
    product_list, promotion_product_list, first_promotion_product, pagination = get_product(page)

    return render(request, 'main/ai_solutions.html',
                  {'product_list': product_list, 'promotion_product_list': promotion_product_list,
                   'first_promotion_product': first_promotion_product,
                   'promotion_product_num': range(len(promotion_product_list)),
                   'pagination': pagination})


#@check_login
def smart_curtain(request, page):
    product_list, promotion_product_list, first_promotion_product, pagination = get_product(page)
    load_animation = False
    if page == 1:
        load_animation = True

    return render(request, 'main/smart_curtain.html',
                  {'product_list': product_list,
                   'pagination': pagination,
                   'load_animation': load_animation})


#@check_login
def virtual_reality_forest_therapy(request, page):
    product_list, VR_product_list, first_promotion_product, pagination = get_VR_FT(page)
    
    return render(request, 'main/virtual_reality_forest_therapy.html',
                  {'product_list': product_list, 'VR_product_list': VR_product_list,
                   'first_promotion_product': first_promotion_product,
                   'promotion_product_num': range(len(VR_product_list)),
                   'pagination': pagination})

def digital_twin(request):
    return render(request,'main/VRFT_digital_twin.html')

def vr_detail(request):
    return render(request,'main/vr_detail.html')